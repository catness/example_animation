# About the project

**ExampleAnimation** is an example for [Advanced Android in Kotlin 03.1: Property Animation](https://codelabs.developers.google.com/codelabs/advanced-android-kotlin-training-property-animation). It uses ObjectAnimator with several properties (via PropertyValuesHolder) to animate a sprite walking from one corner of the screen to another. AnimatorSet is used to animate the sprite and the screen color together. 

Sprite is animated with AnimationDrawable (which wasn't in the codelab but I found it).

Sprite graphics is from https://opengameart.org/content/2d-character-animation-sprite .


The apk is here: [exampleanimation.apk](apk/exampleanimation.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
