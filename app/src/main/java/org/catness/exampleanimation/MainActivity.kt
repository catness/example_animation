package org.catness.exampleanimation

import android.animation.*
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.AnimationDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.WindowManager
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.animation.addListener
import androidx.core.animation.doOnRepeat

class MainActivity : AppCompatActivity() {
    private lateinit var walkAnimation: AnimationDrawable
    private lateinit var sprite: ImageView
    private lateinit var frame: FrameLayout
    private lateinit var goButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        frame = findViewById(R.id.frame)
        sprite = findViewById(R.id.sprite)

        sprite.setBackgroundResource(R.drawable.standing_000)
        goButton = findViewById<Button>(R.id.goButton)

        goButton.setOnClickListener {
            translater()
        }

    }

    private fun translater() {

        val height = frame.height.toFloat()
        val spriteHeight = sprite.height.toFloat()
        val width = frame.width.toFloat()
        val spriteWidth = sprite.width.toFloat()
        Log.i("MAIN", "height = $height spriteHeight=$spriteHeight")
        val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X, 0.5f)
        val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 0.5f)
        val x = PropertyValuesHolder.ofFloat(TRANSLATION_X, (width - spriteWidth * 2 / 3))
        val y = PropertyValuesHolder.ofFloat(TRANSLATION_Y, -height + spriteHeight * 2 / 3)
        val alpha = PropertyValuesHolder.ofFloat(View.ALPHA, 0.4f)
        val duration = 5000L

        val animator = ObjectAnimator.ofPropertyValuesHolder(
                sprite, x, y, scaleX, scaleY, alpha
        )
        animator.repeatCount = 1
        animator.repeatMode = ObjectAnimator.REVERSE
        animator.disableViewDuringAnimation(goButton)

        var animatorBG = ObjectAnimator.ofArgb(
                frame,
                "backgroundColor",
                // It doesn't work with R.color.* - colors are all wrong!
                Color.rgb(0x03, 0xda, 0xc5),
                Color.rgb(0x92, 0xa7, 0xbd)
        )
        animatorBG.repeatCount = 1
        animatorBG.repeatMode = ObjectAnimator.REVERSE

        val set = AnimatorSet()
        set.playTogether(animator, animatorBG)
        set.duration = duration

        // if it's set.addListener instead, then onAnimationRepeat doesn't get called!
        // start and end still work

        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                Log.i("MAIN", "Start!")
                sprite.apply {
                    setBackgroundResource(R.drawable.walkanim)
                    walkAnimation = background as AnimationDrawable
                }
                walkAnimation.start()
            }

            override fun onAnimationRepeat(animation: Animator?) {
                Log.i("MAIN", "Repeat!")
                sprite.rotationY = 180f
            }

            override fun onAnimationEnd(animation: Animator?) {
                Log.i("MAIN", "End!")
                walkAnimation.stop()
                sprite.rotationY = 0f
                sprite.setBackgroundResource(R.drawable.standing_000)
            }
        })
        set.start()
    }

    private fun ObjectAnimator.disableViewDuringAnimation(view: View) {
        addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                view.isEnabled = false
            }

            override fun onAnimationEnd(animation: Animator?) {
                view.isEnabled = true
            }
        })
    }
}